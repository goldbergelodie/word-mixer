# word-mixer

Word-mixer is a tool aimed to help find new experimental translations to terms that have been coined or theorized mostly in English.
*Word-mixer est un outil destiné à trouver de nouvelles traductions expérimentales à des termes qui ont été théorisés majoritairement en anglais.*


![interface illustration](/img/screenshot.png)


