import os

from flask import Flask, render_template, request, jsonify
import pymysql

def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'flaskr.sqlite'),
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    db = pymysql.connect(
    host='localhost',
    user='elo',
    password='elo',
    db='word-mixer'
    )

 
   
    @app.route('/')
    def index():

        cursor = db.cursor()

        cursor.execute("SELECT * FROM english_term")
        result = cursor.fetchall()

        terms = []

        for item in result:
            terms.append([item[0], item[1], item[2]])



        return render_template('index.html', terms=terms)

    @app.route('/term', methods=['POST'])
    def term():
        jsonData = request.get_json(force=True)
        data = jsonData

        word_id = data['id']
        word = data['word']
        word_definition = data['definition']

        # print(word_definition)
                        
        cursor = db.cursor()

        cursor.execute("SELECT label FROM french_translations WHERE english_term_id=(%s)", (word_id))
        translations = cursor.fetchall()

        return jsonify(success=True, data=data, translations=translations)

    @app.route('/split', methods=['POST'])
    def split():
        jsonData = request.get_json(force=True)
        other_data = jsonData

        print(other_data)

        return jsonify(success=True, other_data= other_data)
    
    return app